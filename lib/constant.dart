import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF73A1C2);
const kPrimaryLightColor = Color(0xFFEBFBF9);
const kTextColor = Color(0xFF186AA5);
const kBackgroundcolor = Color(0xFFF6F6F6);
const coloricon1 = Color(0xFF8E0C3A);
const colortext = Color(0xFFFFFFFF);
