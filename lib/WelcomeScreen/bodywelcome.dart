import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sapa_apps/constant.dart';

class BodyWelcome extends StatelessWidget {
  const BodyWelcome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Image.asset(
            "assets/images/welcome1.png",
            height: 200.0,
            width: 300.0,
          ),
        ),
        SizedBox(
          height: size.height * 0.02,
        ),
        SvgPicture.asset(
          "assets/images/tulisansapa.svg",
        ),
        SizedBox(
          height: size.height * 0.02,
        ),
        Text(
          "KAMI ADA UNTUK MEMBANTU",
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
              color: colortext,
              fontFamily: 'Montserrat-Black'),
        )
      ],
    );
  }
}
