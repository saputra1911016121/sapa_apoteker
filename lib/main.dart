import 'package:flutter/material.dart';
import 'package:sapa_apps/Launcher/launcher.dart';
import 'package:sapa_apps/constant.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sapa Apoteker',
      theme: ThemeData(
          primaryColor: kPrimaryColor, scaffoldBackgroundColor: kPrimaryColor),
      home: const LauncherPage(),
    );
  }
}
